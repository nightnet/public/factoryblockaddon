# FactoryBlock Addon
## 1. General Information
| Data | Value |
|------|-------|
| Name | FactoryBlockAddon|
| Server-Version | 1.20 |
| Depends | Capi |
| Soft Depends | Vault |

## 2. Commands
### Detector Commands:
**Syntax:**

```markdown
/fb <listblocktypes | find> [<blocktype> <radius>]
```

**Examples:**

- To list all configured blocktypes in the config:

```markdown
/fb list blocktypes
```

- To find all special blocks near you within a given radius:

```markdown
/fb find <blocktype> <radius>
```

---

## 3. Configs
The Configuration files are highly documented in the files.

## 4. BlockBreaker

The BlockBreaker is a special BlockType, that can be configured in the blockBreaker.yml file.
It works by powering it with a redstone signal. If powered it breaks the block in front of the breaker. For each redstone activation, the breaker will break one single block. A constant power signal will cause only one block to break.

## 5. RedstoneSigns

The RedstoneSign is a unique sign identified by [RedstoneSign] in the first line. To activate it, simply right-click on it. Upon doing so, the amount written on the sign will be transferred to the owner, and a Redstone Block will be placed at the location specified on the sign. After the specified duration mentioned on the sign, the Redstone Block will be removed and replaced with the original block that was there before.